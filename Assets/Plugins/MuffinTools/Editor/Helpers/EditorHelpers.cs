﻿using System;

using UnityEngine;
using UnityEngine.Events;

using UnityEditor;
using UnityEditor.AnimatedValues;

namespace MuffinTools
{

///<summary>
/// 
///</summary>
public static class EditorHelpers
{

	#region Attributes

		// Constants

		private const string c_Assets = "Assets";
		private const string c_Resources = "Resources";

		// Statics

		public static readonly Rect s_0To1Curve = new Rect(0.0f, 0.0f, 1.0f, 1.0f);

	#endregion


	#region Initialization

		/// <summary>
		/// Initializes a given Anim Bool with a given value.
		/// The listener is usually Repaint.
		/// </summary>
		/// <param name="_Listener">The listener is usually Repaint.</param>
		public static void InitAnimBool(out AnimBool _AnimBoolTarget, bool _Value, UnityAction _Listener)
		{
			_AnimBoolTarget = new AnimBool(_Value);

			_AnimBoolTarget.valueChanged.AddListener(_Listener);
		}

		/// <summary>
		/// Use FindProperty() to find the given property in the given object.
		/// Log an error if that property has not been found.
		/// </summary>
		/// <returns>Returns the found property or null if the object is null
		/// or the property has not been found.</returns>
		public static SerializedProperty FindProperty(SerializedObject _Object, string _PropertyName)
		{
			if(_Object != null)
			{
				SerializedProperty property = _Object.FindProperty(_PropertyName);

				if(property != null)
				{
					return property;
				}

				else
				{
					Debug.LogWarning("ERROR from EditorHelpers.FindProperty() : The given Serialized Object has no property '" + _PropertyName + "'.");
				}
			}

			else
			{
				Debug.LogWarning("ERROR from EditorHelpers.FindProperty() : The given Serialized Object is null.");
			}

			return null;
		}

	#endregion

	
	#region Assets

		/// <summary>
		/// Creates a ScriptableObject as an asset.
		/// </summary>
		/// <param name="_AssetToCreateType">The type of the Scriptable Object to create.</param>
		/// <param name="_RelativePath">This path needs to be from to the current Project's
		/// "Assets" folder. Note that the "Assets" folder musn't be included.
		/// <param name="_FocusAsset">If this option is true, the new created asset will be focused
		/// by the Selection and the Project window.</param>
		public static AssetCreationResult CreateAsset
		(
			Type	_AssetToCreateType,
			string	_FileName			= "NewAsset",
			string	_RelativePath		= "",
			string	_FileExtension		= "asset",
			bool	_FocusAsset			= true
		)
		{
			if(_AssetToCreateType != null)
			{
				string assetPath = c_Assets + "/";
				assetPath += (_RelativePath != string.Empty) ? _RelativePath + "/" : string.Empty;
				assetPath += _FileName + "." + _FileExtension;

				// Create the asset of the given type, then save it.
				ScriptableObject newAsset = ScriptableObject.CreateInstance(_AssetToCreateType);

				if (newAsset != null)
				{
					AssetDatabase.CreateAsset(newAsset, assetPath);
					AssetDatabase.SaveAssets();

					if (_FocusAsset)
					{
						Selection.activeObject = newAsset;
						EditorUtility.FocusProjectWindow();
						EditorGUIUtility.PingObject(newAsset);
					}

					string absolutePath = Application.dataPath + assetPath.Substring(c_Assets.Length);
					return new AssetCreationResult(absolutePath, newAsset);
				}

				else
				{
					Debug.LogWarning("ERROR from EditorHelpers.CreateAsset() : Impossible to create a ScriptableObject from Type '" + _AssetToCreateType + "\'.");
				}
			}

			else
			{
				Debug.LogWarning("ERROR from EditorHelpers.CreateAsset() : The given Type is null.");
			}

			return AssetCreationResult.s_FailedAssetCreation;
		}

		public static AssetCreationResult CreateAsset<T>
		(
			string	_FileName			= "NewAsset",
			string	_RelativePath		= "",
			string	_FileExtension		= "asset",
			bool	_FocusAsset			= true
		)
		{
			return CreateAsset
			(
				typeof(T),
				_FileName,
				_RelativePath,
				_FileExtension,
				_FocusAsset
			);
		}

		/// <summary>
		/// Creates a ScriptableObject as an asset, using a Save File Panel to define the path to
		/// that new asset.
		/// </summary>
		/// <param name="_AssetToCreateType">The type of the Scriptable Object to create.</param>
		/// <param name="_FocusAsset">If this option is true, the new created asset will be focused
		/// by the Selection and the Project window.</param>
		public static AssetCreationResult CreateAssetPanel
		(
			Type	_AssetToCreateType,
			string	_PanelTitle,
			string	_DefaultFileName		= "NewAsset",
			string	_DefaultPath			= "",
			string	_DefaultFileExtension	= "asset",
			bool	_FocusAsset				= true
		)
		{
			// Open a SaveFilePanel with the given settings.
			// We'll get an absolute path that will be the selected path to the file to create.
			string absolutePath = EditorUtility.SaveFilePanel(_PanelTitle, _DefaultPath, _DefaultFileName, _DefaultFileExtension);

			// If the user close the Save File Panel, the path will be Empty.
			// Then, we can stop this method.
			if (absolutePath == string.Empty)
				return AssetCreationResult.s_FailedAssetCreation;

			// If the selected path is in the current project's folder.
			if (absolutePath.StartsWith(Application.dataPath))
			{
				// The path becomes relative, from the excluded Assets folder.
				string relativePath = absolutePath.Substring(Application.dataPath.Length - c_Assets.Length);

				// Create the asset of the given type, then save it.
				ScriptableObject newAsset = ScriptableObject.CreateInstance(_AssetToCreateType);

				AssetDatabase.CreateAsset(newAsset, relativePath);
				AssetDatabase.SaveAssets();

				// If we want to focus the created asset after its creation :
				// Set it as the active selection, focus the Project window and highlight (ping) it.
				if(_FocusAsset)
				{
					Selection.activeObject = newAsset;
					EditorUtility.FocusProjectWindow();
					EditorGUIUtility.PingObject(newAsset);
				}

				return new AssetCreationResult(absolutePath, newAsset);
			}

			else
			{
				Debug.LogWarning("ERROR from EditorHelpers.CreateAsset() : You must select a folder relative to the \"Assets\" folder of this project.");

				return AssetCreationResult.s_FailedAssetCreation;
			}
		}

		/// <summary>
		/// Creates a ScriptableObject as an asset, using a Save File Panel to define the path to
		/// that new asset.
		/// </summary>
		/// <typeparam name="T">The type of the Scriptable Object to create.</param>
		/// <param name="_FocusAsset">If this option is true, the new created asset will be focused
		/// by the Selection and the Project window.</param>
		public static AssetCreationResult CreateAssetPanel<T>
		(
			string	_PanelTitle,
			string	_DefaultFileName		= "NewAsset",
			string	_DefaultPath			= "",
			string	_DefaultFileExtension	= "asset",
			bool	_FocusAsset				= true
		)
			where T : ScriptableObject
		{
			return CreateAssetPanel
			(
				typeof(T),
				_PanelTitle,
				_DefaultFileName,
				_DefaultPath,
				_DefaultFileExtension,
				_FocusAsset
			);
		}

        #endregion


    #region Paths

        public static bool IsPathRelativeToCurrentProjectFolder(string _AbsolutePath)
        {
            return _AbsolutePath.StartsWith(Application.dataPath);
        }

        /// <summary>
        /// Get a path relative to this project's Assets folder from a
        /// given absolute path.
        /// NOTE : This method will write a log if the given path is not
        /// valid.
        /// </summary>
        /// <param name="_IncludeAssetsFolder">If true, the relative path
        /// will start with "Assets/". If false, the Assets folder will be
        /// ignorded.</param>
        /// <returns>Returns the relative path if the given absolute path
        /// starts with the absolute path of this project's assets folder,
        /// or returns string.Empty if not.</returns>
        public static string GetPathRelativeToCurrentProjectFolder(string _AbsolutePath, bool _IncludeAssetsFolder = true)
		{
			// Check if the given path is in the current project's folder.
			if (_AbsolutePath.StartsWith(Application.dataPath))
			{
				int substringStartIndex = Application.dataPath.Length;

				if(_IncludeAssetsFolder)
				{
					substringStartIndex -= c_Assets.Length;
				}

				return _AbsolutePath.Substring(substringStartIndex);
			}

			else
			{
				Debug.LogWarning("ERROR from EditorHelpers.GetPathRelativeToCurrentProjectFolder() : The given path is not pointing on this current project's \"Assets\" folder (" + _AbsolutePath + ").");

				return string.Empty;
			}
		}

		/// <summary>
		/// Gets the absolute path to an asset.
		/// </summary>
		public static string GetAssetAbsolutePath(UnityEngine.Object _Asset)
		{
			return GetAssetAbsolutePath(_Asset.GetInstanceID());
		}

		/// <summary>
		/// Gets the absolute path to an asset.
		/// </summary>
		/// <returns>Returns the absolute path, or string.Empty if the asset doesn't exist.</returns>
		public static string GetAssetAbsolutePath(int _InstanceID)
		{
			string relPath = AssetDatabase.GetAssetPath(_InstanceID);
			return GetAssetAbsolutePath(relPath);
		}

		/// <summary>
		/// Gets the absolute path to an asset from a given relative path.
		/// </summary>
		public static string GetAssetAbsolutePath(string _RelativePath)
		{
			if (_RelativePath == string.Empty)
			{
				return string.Empty;
			}

			string absPath = Application.dataPath;
			absPath = absPath.Substring(0, absPath.Length - c_Assets.Length);

			return absPath + _RelativePath;
		}

		/// <summary>
		/// Use GetPathRelativeToCurrentProjectFolder() to convert the absolute path
		/// in a relative path.
		/// Then, split that path by the separator "/". Keep only the path in the
		/// resources folder if there's one in the path.
		/// </summary>
		/// <returns>Returns the resources path if everything is ok, or string.Empty
		/// if the absolute path doen't point in the current project's Assets folder,
		/// or if there's no Resources folder in the given Path.</returns>
		public static string GetResourcesPath(string _AbsolutePath, string _ExpectedExtension, bool _IncludeResourcesFolder = false)
		{
			string relPath = GetPathRelativeToCurrentProjectFolder(_AbsolutePath, false);
			string resourcesPath = string.Empty;

			if(relPath != string.Empty)
			{
				string[] splitPath = relPath.Split('/');

				int count = splitPath.Length;

				bool resourcesFound = false;

				for (int i = 0; i < count; i++)
				{
					if (!resourcesFound)
					{
						if (splitPath[i] == c_Resources)
						{
							if (_IncludeResourcesFolder)
							{
								resourcesPath += c_Resources;
							}

							resourcesFound = true;
						}
					}

					else
					{
						resourcesPath += (resourcesPath == string.Empty) ? splitPath[i] : "/" + splitPath[i];
					}
				}

				if(!resourcesFound)
				{
					Debug.LogWarning("ERROR from EditorHelpers.GetResourcesPath() : The given path doesn't point to a Resources folder.");
				}
			}

			Debug.Log("resourcesPath = " + resourcesPath);

			if(_ExpectedExtension != string.Empty)
			{
				return resourcesPath.Substring(0, resourcesPath.Length - (_ExpectedExtension.Length + 1));
			}

			return resourcesPath;
		}

	#endregion

}

}