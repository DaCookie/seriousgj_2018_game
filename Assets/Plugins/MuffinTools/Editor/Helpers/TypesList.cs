﻿using System;
using System.Reflection;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

namespace MuffinTools
{

/*
 * This class allows you to make a Popup in Editor to select a Type
 * in a list.
 * 	
 * 		You have just to set the Assembly where this class will get the
 * 	Types. You can do it at its construction, or using SetAssemblyToCheck().
 * 	
 * 		You can limit the search of Types in this Types List's Assembly by
 * 	adding "Parent Types".
 * 		If this list of Parent Types is empty, all the Types of the Assembly
 * 	will be included in the list. If there's at least one Parent Type added
 * 	to this Types List, only the types that are inheritors of these Parent
 * 	Types will be included in the list.
 * 	
 * 		You can set Types that won't be included to the Types list. To do
 * 	it, just use AddTypeToAvoid().
 * 	
 * 		TIP : If you want to get the Types that inherit from certain parents
 * 	but you don't want to include the parent, you must add the parent types
 * 	as Types To Avoid.
 * 	
 * 		You can display the Types List using DrawTypesList(). This method
 * 	returns the currently selected Type name. But you can register for
 * 	delegates to get it by another way.
 * 		The OnChangeSelectedType delegate is called when the selected type
 * 	is changed normally.
 * 		The OnForceChangeSelectedType delegate is called when the Types List
 * 	won't be able to select the previously selected type. It can be because
 * 	you gave an empty Type name to DrawTypesList(), or because the given
 * 	Type name is not in the list.
 * 	
 * 		By default, DrawTypesList() will display the list using
 * 	EditorGUILayout.Popup(int, string[]). If you want to draw that list using
 * 	a label, or EditorGUI for example, you can define the method to use to
 * 	draw the list.
 * 		You need to create a method with the prototype of the DrawListDelegate :
 * 			int DrawListDelegate(int _SelectedIndex, string[] _TypeNamesList)
 * 		You can set it to a TypesList using SetDrawListMethod(). Then, the
 * 	DrawTypesList() method will use YOUR method to draw the list.
 * 	
 * 		Note that if there's no Type to display, the DrawTypesList() will have
 * 	a special behaviour. By default, a message "There's no Type to display." is
 * 	displayed using EditorGUILayout.HelpBox().
 * 		For this case too, you can set the method to use for this feedback. Create
 * 	a method with the DrawNoTypeToDisplayDelegate prototype :
 * 			void DrawNoTypeToDisplayDelegate()
 * 		Then, set it to this TypesList using SetDrawNoTypeToDispayMethod().
 */

///<summary>
///
///		Creates a list of Types, contained in a given Assembly.
///		That list can be displayed in Editor using DrawTypesList().
///		
///</summary>
public sealed class TypesList
{

	#region Attributes

		// Delegates

		public delegate void ChangeSelectedTypeDelegate(string _NewSelectedTypeName);
		public delegate int DrawListDelegate(int _SelectedIndex, string[] _TypeNamesList);
		public delegate void DrawNoTypeToDisplayDelegate();

		// Called when the selected type of the list is changed.
		public ChangeSelectedTypeDelegate OnChangeSelectedType;

		// Called if there's no selected type or if the currently selected type doesn't exist.
		public ChangeSelectedTypeDelegate OnForceChangeSelectedType;

		// Called when the DrawTypesList() method is called.
		private DrawListDelegate DrawListMethod;

		private DrawNoTypeToDisplayDelegate DrawNoTypeToDisplayMethod;

		// Constants

		public const string c_CSharpScriptsAssemblyName = "Assembly-CSharp";

		private const string c_DefaultAssemlyToCheckName = "NULL Assembly";

		// Resources

		private Assembly m_AssemblyToCheck = null;

		private List<Type> m_ParentTypes = new List<Type>();
		private List<Type> m_TypesToAvoid = new List<Type>();

	#endregion


	#region Initialization

		/// <summary>
		/// Create a Types List without any Assembly to check.
		/// You can add Assemblies where to get Types using AddAssemblyToCheck().
		/// </summary>
		public TypesList()
		{
			InitDelegates();
		}

		/// <summary>
		/// Create a Types List that will get Types that are only in the given Assembly.
		/// </summary>
		public TypesList(Assembly _AssemblyToCheck)
		{
			SetAssemblyToCheck(_AssemblyToCheck);

			InitDelegates();
		}

		/// <summary>
		/// Create a Types List that will get Types that are only in the named Assembly.
		/// </summary>
		public TypesList(string _AssemblyNameToCheck)
		{
			SetAssemblyToCheck(_AssemblyNameToCheck);

			InitDelegates();
		}

		/// <summary>
		/// Create a Types List that will get Types that inherit of the given Parent Type,
		/// and that are only in the given Assembly.
		/// </summary>
		public TypesList(Assembly _AssemblyToCheck, Type _ParentType)
		{
			SetAssemblyToCheck(_AssemblyToCheck);
			AddParentClass(_ParentType);

			InitDelegates();
		}

		/// <summary>
		/// Create a Types List that will get Types that inherit of the given Parent Type,
		/// and that are only in the named Assembly.
		/// </summary>
		public TypesList(string _AssemblyNameToCheck, Type _ParentType)
		{
			SetAssemblyToCheck(_AssemblyNameToCheck);
			AddParentClass(_ParentType);

			InitDelegates();
		}

		/// <summary>
		/// Create a Types List that will get Types that inherit of the given Parent Types,
		/// and that are only in the given Assemblies.
		/// </summary>
		public TypesList(Assembly _AssemblyToCheck, Type[] _ParentTypes)
		{
			SetAssemblyToCheck(_AssemblyToCheck);
			AddParentClasses(_ParentTypes);

			InitDelegates();
		}

		/// <summary>
		/// Create a Types List that will get Types that inherit of the given Parent Types,
		/// and that are only in the named Assemblies.
		/// </summary>
		public TypesList(string _AssemblyNameToCheck, Type[] _ParentTypes)
		{
			SetAssemblyToCheck(_AssemblyNameToCheck);
			AddParentClasses(_ParentTypes);

			InitDelegates();
		}

		/// <summary>
		/// Set the default delegates for :
		///		- List drawing
		///		- "No type to display" message drawing
		/// </summary>
		private void InitDelegates()
		{
			DrawListMethod = DrawPopup;
			DrawNoTypeToDisplayMethod = DrawNoTypeToDisplay;
		}

	#endregion


	#region Public Methods

		/// <summary>
		/// Add the given Type as a parent class in the list if it's not already in.
		/// 
		/// Note that if there's no Parent class in the list, all the Assembly of the
		/// list will be checked. If there's at least one parent Type in the list, only
		/// the inheritors of one of these types will be displayed in the list.
		/// </summary>
		public void AddParentClass(Type _ParentToAdd)
		{
			if(_ParentToAdd != null)
			{
				if(!IsTypeInAssembly(m_AssemblyToCheck, _ParentToAdd))
				{
					Debug.Log("MESSAGE from TypesList.AddParentClass() : The Type you want to add ('" + _ParentToAdd.Name + "') is not in this TypesList Assembly ('" + AssemblyToCheckName + "').");
					return;
				}

				// Check if the given type is not already in the list.
				if(m_ParentTypes.Contains(_ParentToAdd))
				{
					Debug.Log("MESSAGE from TypesList.AddParentClass() : The Type you want to add ('" + _ParentToAdd.Name + "') is already in the list.");
					return;
				}

				// Check if the given Type has one of its parents already in the list.
				if (ReflectionHelpers.IsInheritorOf(_ParentToAdd, m_ParentTypes))
				{
					Debug.Log("MESSAGE from TypesList.AddParentClass() : There's already a parent of the given type ('" + _ParentToAdd.Name + "') in the list.");
					return;
				}

				// Add the given Type in the list if its not already in.
				m_ParentTypes.Add(_ParentToAdd);
			}

			else
			{
				Debug.LogWarning("ERROR from TypesList.AddParentClass() : The given Type is null.");
			}
		}

		/// <summary>
		/// Add the named Type as a parent class in the list if it's not already in.
		/// 
		/// Note that if there's no Parent class in the list, all the Assembly of the
		/// list will be checked. If there's at least one parent Type in the list, only
		/// the inheritors of one of these types will be displayed in the list.
		/// </summary>
		public void AddParentClass(string _ParentFullNameToAdd)
		{
			AddParentClass(GetTypeOfName(_ParentFullNameToAdd));
		}

		/// <summary>
		/// Add the given Types as parent classes in the list if they're not already in.
		/// 
		/// Note that if there's no Parent class in the list, all the Assembly of the
		/// list will be checked. If there's at least one parent Type in the list, only
		/// the inheritors of one of these types will be displayed in the list.
		/// </summary>
		public void AddParentClasses(IEnumerable<Type> _ParentTypes)
		{
			foreach(Type type in _ParentTypes)
			{
				AddParentClass(type);
			}
		}

		/// <summary>
		/// Add the named Types as parent classes in the list if they're not already in.
		/// 
		/// Note that if there's no Parent class in the list, all the Assembly of the
		/// list will be checked. If there's at least one parent Type in the list, only
		/// the inheritors of one of these types will be displayed in the list.
		/// </summary>
		public void AddParentClasses(IEnumerable<string> _ParentFullNameTypes)
		{
			foreach (string typeName in _ParentFullNameTypes)
			{
				AddParentClass(typeName);
			}
		}

		/// <summary>
		/// Removes the given Type from the Parent Types list.
		/// 
		/// Note that if there's no Parent class in the list, all the Assembly of the
		/// list will be checked. If there's at least one parent Type in the list, only
		/// the inheritors of one of these types will be displayed in the list.
		/// </summary>
		public void RemoveParentClass(Type _ParentToRemove)
		{
			if (_ParentToRemove != null)
			{
				int index = m_ParentTypes.IndexOf(_ParentToRemove);

				if (index != -1)
				{
					m_ParentTypes.RemoveAt(index);
				}

				else
				{
					Debug.Log("MESSAGE from TypesList.RemoveParentClass() : The Type '" + _ParentToRemove.Name + "' is not in the Parent Types list.");
				}
			}

			else
			{
				Debug.LogWarning("ERROR from TypesList.RemoveParentClass() : The given Type is null.");
			}
		}

		/// <summary>
		/// Removes the named Type from the Parent Types list.
		/// 
		/// Note that if there's no Parent class in the list, all the Assembly of the
		/// list will be checked. If there's at least one parent Type in the list, only
		/// the inheritors of one of these types will be displayed in the list.
		/// </summary>
		public void RemoveParentClass(string _ParentFullNameToRemove)
		{
			RemoveParentClass(GetTypeOfName(_ParentFullNameToRemove));
		}

		/// <summary>
		/// Clear the list of Parent classes.
		/// </summary>
		public void ClearParentClasses()
		{
			m_ParentTypes.Clear();
		}

		/// <summary>
		/// Add the given Type in the Types To Avoid list if it's not already in.
		/// </summary>
		public void AddTypeToAvoid(Type _Type)
		{
			if (_Type != null)
			{
				if (!IsTypeInAssembly(m_AssemblyToCheck, _Type))
				{
					Debug.Log("MESSAGE from TypesList.AddTypeToAvoid() : The Type you want to add ('" + _Type.Name + "') is not in this TypesList Assembly ('" + AssemblyToCheckName + "').");
					return;
				}

				// Check if the given type is not already in the list.
				if (m_TypesToAvoid.Contains(_Type))
				{
					Debug.Log("MESSAGE from TypesList.AddTypeToAvoid() : The Type you want to add ('" + _Type.Name + "') is already in the list.");
					return;
				}

				// Add the given Type in the list if its not already in.
				m_TypesToAvoid.Add(_Type);
			}

			else
			{
				Debug.LogWarning("ERROR from TypesList.AddTypeToAvoid() : The given Type is null.");
			}
		}

		/// <summary>
		/// Add the named Type in the Types To Avoid list if it's not already in.
		/// </summary>
		public void AddTypeToAvoid(string _TypeFullName)
		{
			AddTypeToAvoid(GetTypeOfName(_TypeFullName));
		}

		/// <summary>
		/// Add all the given Types in the Types To Avoid list if  they're not already in.
		/// </summary>
		public void AddTypesToAvoid(IEnumerable<Type> _Types)
		{
			foreach (Type type in _Types)
			{
				AddTypeToAvoid(type);
			}
		}

		/// <summary>
		/// Add all the named Types in the Types To Avoid list if  they're not already in.
		/// </summary>
		public void AddTypesToAvoid(IEnumerable<string> _TypesFullNames)
		{
			foreach (string typeName in _TypesFullNames)
			{
				AddTypeToAvoid(typeName);
			}
		}

		/// <summary>
		/// Removes the given Type in the Types To Avoid list.
		/// </summary>
		public void RemoveTypeToAvoid(Type _Type)
		{
			if (_Type != null)
			{
				int index = m_TypesToAvoid.IndexOf(_Type);

				if (index != -1)
				{
					m_TypesToAvoid.RemoveAt(index);
				}

				else
				{
					Debug.Log("MESSAGE from TypesList.RemoveTypeToAvoid() : The Type '" + _Type.Name + "' is not in the Types To Avoid list.");
				}
			}

			else
			{
				Debug.LogWarning("ERROR from TypesList.RemoveTypeToAvoid() : The given Type is null.");
			}
		}

		/// <summary>
		/// Removes the named Type in the Types To Avoid list.
		/// </summary>
		public void RemoveTypeToAvoid(string _TypeFullName)
		{
			RemoveTypeToAvoid(GetTypeOfName(_TypeFullName));
		}

		/// <summary>
		/// Clear the Types To Avoid list.
		/// </summary>
		public void ClearTypesToAvoid()
		{
			m_TypesToAvoid.Clear();
		}

		/// <summary>
		/// Refresh the list and displays it in Editor.
		/// </summary>
		/// <returns>Returns the selected type.</returns>
		public string DrawTypesList(string _LastSelectedTypeName)
		{
			string[] typesList = GetTypeNamesList(m_AssemblyToCheck, m_ParentTypes, m_TypesToAvoid);

			if(typesList.Length > 0)
			{
				int lastSelectedTypeIndex = GetIndexOf(_LastSelectedTypeName, typesList);

				// If the given selected Type is empty or not in the Types list.
				if(lastSelectedTypeIndex == -1)
				{
					lastSelectedTypeIndex = 0;
					CallChangeSelectedTypeDelegate(OnForceChangeSelectedType, typesList[0]);
				}

				int newSelectedTypeIndex = DrawListMethod(lastSelectedTypeIndex, typesList);

				// If the user selects another Type
				if(newSelectedTypeIndex != lastSelectedTypeIndex)
				{
					CallChangeSelectedTypeDelegate(OnChangeSelectedType, typesList[newSelectedTypeIndex]);
				}

				_LastSelectedTypeName = typesList[newSelectedTypeIndex];
				return _LastSelectedTypeName;
			}

			else
			{
				DrawNoTypeToDisplayMethod();
				return string.Empty;
			}
		}

	#endregion


	#region Private Methods
	
		/// <summary>
		/// Add all the found Types' name in the given Type Names List.
		/// </summary>
		private void AddTypesFromAssembly(Assembly _Assembly, List<Type> _ParentTypes, List<Type> _TypesToAvoid, List<string> _TypeNamesList)
		{
			if(_Assembly != null)
			{
				Type[] assemblyTypes = _Assembly.GetTypes();

				int assemblyTypesCount = assemblyTypes.Length;

				// We want to add only inheritors of parent classes if teher's at least one of them.
				bool checkIfInheritor = (_ParentTypes.Count > 0);

				for(int i = 0; i < assemblyTypesCount; i++)
				{
					// If the current Type is not Abstract and not Generic.
					if(IsValidType(assemblyTypes[i]))
					{
						// If the current Type is not a Type to avoid.
						if (!_TypesToAvoid.Contains(assemblyTypes[i]))
						{
							// If we want all the types of the Assembly or if the current Type inherits from a ParentType.
							if (!checkIfInheritor || ReflectionHelpers.IsInheritorOf(assemblyTypes[i], _ParentTypes))
							{
								_TypeNamesList.Add(assemblyTypes[i].FullName);
							}
						}
					}
				}
			}

			else
			{
				Debug.Log("MESSAGE from TypesList.AddTypesFromAssembly() : The given Assembly is null.");
			}
		}

		/// <summary>
		/// Draw a Popup, using EditorGUILayout.Popup(int, string[]).
		/// </summary>
		/// <returns>Returns the selected index in the drawn Popup.</returns>
		private int DrawPopup(int _SelectedIndex, string[] _TypesList)
		{
			return EditorGUILayout.Popup(_SelectedIndex, _TypesList);
		}

		/// <summary>
		/// Draw a Help Box, using EditorGUILayout.HelpBox().
		/// </summary>
		private void DrawNoTypeToDisplay()
		{
			EditorGUILayout.HelpBox("There's no Type to display.", MessageType.None);
		}

		/// <summary>
		/// Call the given delegate if it's not null.
		/// </summary>
		private void CallChangeSelectedTypeDelegate(ChangeSelectedTypeDelegate _Delegate, string _NewType)
		{
			if(_Delegate != null)
			{
				_Delegate(_NewType);
			}
		}

		/// <summary>
		/// Checks if the given type is valid :
		///		- Not abstract
		///		- Not a Generic class
		/// </summary>
		private bool IsValidType(Type _Type)
		{
			return
			(
				!_Type.IsAbstract &&
				!_Type.IsGenericType
			);
		}

		/// <summary>
		/// Checks if the given Type is in the given Assembly.
		/// </summary>
		/// <returns>Returns true if the given type is in the given assembly, false if
		/// not or if the given Assembly or the givne Type is null.</returns>
		private bool IsTypeInAssembly(Assembly _Assembly, Type _Type)
		{
			if(_Assembly != null && _Type != null)
			{
				Type[] assemblyTypes = _Assembly.GetTypes();

				int count = assemblyTypes.Length;

				for(int i = 0; i < count; i++)
				{
					if (_Type == assemblyTypes[i])
					{
						return true;
					}
				}
			}
			
			return false;
		}

	#endregion


	#region Accessors

		/// <summary>
		/// Gets the Type names list as it should be displayed in Editor.
		/// </summary>
		private string[] GetTypeNamesList(Assembly _AssemblyToCheck, List<Type> _ParentTypes, List<Type> _TypesToAvoid)
		{
			List<string> typeNamesList = new List<string>();

			AddTypesFromAssembly(_AssemblyToCheck, _ParentTypes, _TypesToAvoid, typeNamesList);

			return typeNamesList.ToArray();
		}

		/// <summary>
		/// Gets the Type names list.
		/// Note that the Type names are the full name of these Types.
		/// </summary>
		public List<string> GetTypeNamesList()
		{
			List<string> typeNamesList = new List<string>();

			AddTypesFromAssembly(m_AssemblyToCheck, m_ParentTypes, m_TypesToAvoid, typeNamesList);

			return typeNamesList;
		}

		/// <summary>
		/// Gets the index of the first occurence of the given string in the given
		/// array.
		/// </summary>
		/// <returns>Returns the found index, or -1 if there's no occurence of the
		/// given string in the given array or if the given string is empty.</returns>
		private int GetIndexOf(string _String, string[] _StringArray)
		{
			if(_String != string.Empty)
			{
				int count = _StringArray.Length;

				for (int i = 0; i < count; i++)
				{
					if (_String == _StringArray[i])
					{
						return i;
					}
				}
			}

			return -1;
		}

		/// <summary>
		/// Set the Assembly of this Types List if the given one is not null.
		/// </summary>
		private void SetAssemblyToCheck(Assembly _Assembly)
		{
			if (_Assembly != null)
			{
				m_AssemblyToCheck = _Assembly;
			}

			else
			{
				Debug.LogWarning("ERROR from TypesList.SetAssemblyToCheck() : The given Assembly is null.");
			}
		}

		/// <summary>
		/// Set the Assembly of this Types List if the named one is not null.
		/// </summary>
		private void SetAssemblyToCheck(string _AssemblyName)
		{
			Assembly assembly = ReflectionHelpers.GetAssemblyOfName(_AssemblyName);

			if(assembly != null)
			{
				SetAssemblyToCheck(assembly);
			}

			else
			{
				Debug.LogWarning("ERROR from TypesList.SetAssemblyToCheck() : There's no Assembly named '" + _AssemblyName + "' in this project's solution.");
			}
		}

		/// <summary>
		/// Defines the way that the List will be drawn.
		/// </summary>
		public void SetDrawListMethod(DrawListDelegate _DrawListMethod)
		{
			DrawListMethod = _DrawListMethod;
		}

		/// <summary>
		/// Defines the behaviour to have when the list has no Type to display.
		/// </summary>
		public void SetDrawNoTypeToDispayMethod(DrawNoTypeToDisplayDelegate _DrawNoTypeToDisplayMethod)
		{
			DrawNoTypeToDisplayMethod = _DrawNoTypeToDisplayMethod;
		}

		/// <summary>
		/// Gets the Type relative to the given name, using the current Assembly of this
		/// Types List.
		/// </summary>
		public Type GetTypeOfName(string _TypeName)
		{
			return (Type.GetType(_TypeName + "," + m_AssemblyToCheck));
		}

		public Assembly AssemblyToCheck
		{
			get { return m_AssemblyToCheck; }
		}

		public string AssemblyToCheckName
		{
			get { return (m_AssemblyToCheck != null) ? m_AssemblyToCheck.GetName().Name : c_DefaultAssemlyToCheckName; }
		}

	#endregion

}

}