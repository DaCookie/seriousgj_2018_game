﻿using UnityEngine;

namespace MuffinTools
{

///<summary>
///
///		This struct is used by EditorMethods.CreateAsset<>() method.
/// 
///		You can get all the informations you need about the creation of
/// an asset thanks to this struct.
/// 
///		To check if an asset has successfully been set, you can use the
///	accessor "HasFailed" or direectly write :
///	
///			AssetCreationResult<MyObject> myObjectCreationResult;
///			if (myObjectCreationResult) { }
/// 
///</summary>

public struct AssetCreationResult
{

	#region Attributes

		public static readonly AssetCreationResult s_FailedAssetCreation = new AssetCreationResult(false);

		private string m_AbsolutePathToAsset;
		private string m_RelativePathToAsset;

		private Object m_CreatedAsset;

		private bool m_AssetSuccessfullyCreated;

	#endregion

	
	#region Initialisation / Destruction

		private AssetCreationResult(bool _FailedAssetCreation)
		{
			m_AssetSuccessfullyCreated = false;

			m_CreatedAsset = null;

			m_AbsolutePathToAsset = string.Empty;
			m_RelativePathToAsset = string.Empty;
		}

		public AssetCreationResult(string _AbsolutePathToAsset, Object _CreatedAsset)
		{
			m_AssetSuccessfullyCreated = (_CreatedAsset != null);

			m_CreatedAsset = _CreatedAsset;

			m_AbsolutePathToAsset = _AbsolutePathToAsset;
			m_RelativePathToAsset = EditorHelpers.GetPathRelativeToCurrentProjectFolder(_AbsolutePathToAsset);
		}

	#endregion

	
	#region Accessors

		public T GetCreatedAsset<T>()
			where T : Object
		{
			if(m_CreatedAsset != null)
			{
				return m_CreatedAsset as T;
			}

			return null;
		}

		public Object CreatedAsset
		{
			get { return m_CreatedAsset; }
		}

		public string AbsolutePath
		{
			get { return m_AbsolutePathToAsset; }
		}

		public string RelativePath
		{
			get { return m_RelativePathToAsset; }
		}

		public bool SuccessfullyCreated
		{
			get { return m_AssetSuccessfullyCreated; }
		}

	#endregion


	#region Operators

		/// <summary>
		/// Returns true if the asset has successfully been created, false if not.
		/// </summary>
		public static implicit operator bool(AssetCreationResult _AssetCreationResult)
		{
			return _AssetCreationResult.SuccessfullyCreated;
		}

	#endregion

}

}