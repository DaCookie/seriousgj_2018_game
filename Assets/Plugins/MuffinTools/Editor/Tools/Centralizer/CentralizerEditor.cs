﻿using UnityEngine;
using UnityEditor;

namespace MuffinTools
{

///<summary>
/// 
/// 
/// 
///</summary>
public class CentralizerEditor : Editor
{

	#region Attributes

		// Constants

		private const string c_ContextMenu = "Assets/Instantiate at (0; 0; 0)";

	#endregion


	#region Engine Methods

        [MenuItem(c_ContextMenu, true)]
        private static bool ShowWindowValidation()
        {
            return Selection.gameObjects.Length > 0;
        }

        /// <summary>
		/// Gets the window of this tool, or create it if it's not already open.
		/// </summary>
		[MenuItem(c_ContextMenu)]
		private static void InstantiateAtPos0()
		{
            int count = Selection.gameObjects.Length;
			for(int i = 0; i < count; i++)
			{
				Instantiate(Selection.gameObjects[i], Vector3.zero, Quaternion.identity);
			}
        }

	#endregion


	#region Public Methods
	#endregion


	#region Private Methods
	#endregion

}

}