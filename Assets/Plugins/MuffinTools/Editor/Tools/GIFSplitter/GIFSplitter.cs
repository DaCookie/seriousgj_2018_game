﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

using UnityEngine;
using UnityEditor;

namespace MuffinTools
{

///<summary>
/// 
///		
/// 
///</summary>
public class GIFSplitter : EditorWindow
{

	#region Enums & Subclasses

		private enum ExportType
		{
			JPG,
			PNG
		}
		
	#endregion


	#region Attributes

		// Constants

		private const string c_GIFPathPrefKey = "DCTools_GIFSplitter_GIFPath";
		private const string c_ImagesBaseNamePrefKey = "DCTools_GIFSplitter_ImagesBaseName";
		private const string c_TargetPathPrefKey = "DCTools_GIFSplitter_TargetPath";

			// Menu Items

		private	const string c_MenuItem = "Tools/GIFSplitter";		// Name (and Path) of the tool in menus.
		private	const string c_WindowTitle = "GIFSplitter";			// Title of the tool's window.
					  		 
		private	const int	 c_Priority = 1;						// Priority in menus.

		// Flow

		private int m_NbFrames = 0;

	#endregion


	#region Engine Methods

		private void OnEnable()
		{
			m_NbFrames = CountGIFFrames(GetGIFPath());
		}
		
		/// <summary>
		/// Get the window of this tool, or create it if it's not already open.
		/// </summary>
		[MenuItem(c_MenuItem, false, c_Priority)]
		private static void ShowWindow()
		{
			GIFSplitter window = EditorWindow.GetWindow<GIFSplitter>(false, c_WindowTitle, true) as GIFSplitter;
			window.Show();
		}

		private void OnGUI()
		{
			EditorGUILayout.Space();
			DrawChooseGIFField();

			EditorPrefs.SetString
			(
				c_ImagesBaseNamePrefKey,
				EditorGUILayout.TextField("Images Base Name", GetImagesBaseName())
			);

			EditorGUILayout.Space();
			DrawInfos();

			EditorGUILayout.Space();
			DrawExportButtons();
		}

	#endregion


	#region Public Methods
	#endregion


	#region Protected Methods
	#endregion


	#region Private Methods

		/// <summary>
		/// Determines if the tool's window can be open or not.
		/// If it can't be open, the menu will be disabled.
		/// </summary>
		/// <returns>Returns true if the window can be open, false if not.</returns>
		[MenuItem(c_MenuItem, true, c_Priority)]
		private static bool CanShowWindow()
		{
			return true;
		}

		/// <summary>
		/// Draw export buttons.
		/// </summary>
		private void DrawExportButtons()
		{
			EditorGUILayout.BeginHorizontal();
			{
				if (GUILayout.Button("Export as PNG"))
				{
					ExportAsPNG();
				}

				if (GUILayout.Button("Export as JPG"))
				{
					ExportAsJPG();
				}
			}
			EditorGUILayout.EndHorizontal();
		}

		/// <summary>
		/// Draw "Choose GIF" button and label.
		/// </summary>
		private void DrawChooseGIFField()
		{
			EditorGUILayout.BeginHorizontal();
			{
				if (GUILayout.Button("Choose GIF..."))
				{
					ChooseGIF();
				}

				EditorGUILayout.LabelField(GetGIFPath(), EditorStyles.textField);
			}
			EditorGUILayout.EndHorizontal();
		}

		/// <summary>
		/// Draw messages about the GIF to export.
		/// </summary>
		private void DrawInfos()
		{
			EditorGUILayout.HelpBox("Images name : \"" + GetImagesBaseName() + "XXX\"\nXXX : the iteration number (000-999).", MessageType.None);
			EditorGUILayout.HelpBox("Number of frames : " + m_NbFrames, MessageType.None);
		}

		/// <summary>
		/// Exports the selected GIF as a collection of PNG images.
		/// </summary>
		[MenuItem("Assets/Split GIF/Split as PNG")]
		private static void ExportAsPNG()
		{
			Export(ExportType.PNG);
		}

		/// <summary>
		/// Exports the selected GIF as a collection of JPG images.
		/// </summary>
		[MenuItem("Assets/Split GIF/Split as JPG")]
		private static void ExportAsJPG()
		{
			Export(ExportType.JPG);
		}

		/// <summary>
		/// Activate the Export as PNG option at right-click on an
		/// asset if it's a Texture2D.
		/// </summary>
		[MenuItem("Assets/Split GIF/Split as PNG", true)]
		private static bool CanExportAsPNG()
		{
			Object selected = Selection.activeObject;

			return (selected != null && selected is Texture2D);
		}

		/// <summary>
		/// Activate the Export as JPG option at right-click on an
		/// asset if it's a Texture2D.
		/// </summary>
		[MenuItem("Assets/Split GIF/Split as JPG", true)]
		private static bool CanExportAsJPG()
		{
			Object selected = Selection.activeObject;

			return (selected != null && selected is Texture2D);
		}

		/// <summary>
		/// Get the target path of the exported textures and export the GIF's
		/// frames in that folder, in the given format.
		/// </summary>
		private static void Export(ExportType _ExportType)
		{
			string absPath = EditorUtility.OpenFolderPanel("Target folder for extracted frames", GetTargetFolderPath(), string.Empty);

			// If the window has been cancelled
			if(absPath == string.Empty)
				return;

			string relPath = EditorHelpers.GetPathRelativeToCurrentProjectFolder(absPath, false);

			if(relPath != string.Empty)
			{
				EditorPrefs.SetString(c_TargetPathPrefKey, relPath);

				List<Texture2D> GIFFrames = MakeGIFFrames(GetGIFPath());
				
				int count = GIFFrames.Count;

				if(count > 0)
				{
					switch(_ExportType)
					{
						case ExportType.PNG :
							EncodeToPNG(relPath, GIFFrames);
							break;

						case ExportType.JPG :
							EncodeToJPG(relPath, GIFFrames);
							break;

						default:
							break;
					}

					AssetDatabase.Refresh();
				}

				else
				{
					Debug.LogWarning("ERROR from GIFSplitter.Export() : The file hasn't been found.");
				}
			}
		}

		/// <summary>
		/// Encode the given frames as PNG and save them at the given path.
		/// </summary>
		private static void EncodeToPNG(string _TargetFolderRelativePath, List<Texture2D> _Frames)
		{
			int count = _Frames.Count;

			for (int i = 0; i < count; i++)
			{
				System.IO.File.WriteAllBytes(GetPathForFrame(i, "png"), _Frames[i].EncodeToPNG());
			}
		}

		/// <summary>
		/// Encode the given frames as PNG and save them at the given path.
		/// </summary>
		private static void EncodeToJPG(string _TargetFolderRelativePath, List<Texture2D> _Frames)
		{
			int count = _Frames.Count;

			for (int i = 0; i < count; i++)
			{
				System.IO.File.WriteAllBytes(GetPathForFrame(i, "jpg"), _Frames[i].EncodeToJPG());
			}
		}

		/// <summary>
		/// Invite user to choose a GIF image in its local files.
		/// </summary>
		private void ChooseGIF()
		{
			string absPath = EditorUtility.OpenFilePanel
			(
				"Choose GIF to split",
				GetGIFPath(),
				"gif"
			);

			// If the window has not been canceled
			if(absPath != string.Empty)
			{
				EditorPrefs.SetString(c_GIFPathPrefKey, absPath);
				m_NbFrames = CountGIFFrames(GetGIFPath());
			}
		}

		/// <summary>
		/// Count the number of frames of the GIF image at the given path.
		/// </summary>
		/// <returns>Returns the number of frames of the found GIF, or 0
		/// if the file has not been found.</returns>
		private int CountGIFFrames(string _AbsolutePathToGIF)
		{
			// Avoid an exception from the Image.FromFile method if the file doesn't exist.
			// In that case, we just want to return 0.
			if(File.Exists(_AbsolutePathToGIF))
			{
				Image GIFImage = Image.FromFile(_AbsolutePathToGIF);

				FrameDimension dimension = new FrameDimension(GIFImage.FrameDimensionsList[0]);

				return GIFImage.GetFrameCount(dimension);
			}

			return 0;
		}

		/// <summary>
		/// Create Texture2D for each frame of the GIF at the given path.
		/// </summary>
		/// <returns>Returns the list of created textures. Note that the list
		/// will be empty if the file at the given path is not ending with
		/// ".gif", or if it doesn't exist.</returns>
		private static List<Texture2D> MakeGIFFrames(string _AbsolutePathToGIF)
		{
			List<Texture2D> GIFFrames = new List<Texture2D>();

			// Avoid an exception from the Image.FromFile method if the file doesn't exist.
			if(_AbsolutePathToGIF.EndsWith(".gif") && File.Exists(_AbsolutePathToGIF))
			{
				Image GIFImage = Image.FromFile(_AbsolutePathToGIF);

				FrameDimension dimension = new FrameDimension(GIFImage.FrameDimensionsList[0]);

				// Init loop
				int frameCount = GIFImage.GetFrameCount(dimension);
				int i, x, y = 0;

				// Generate a new Texture from each frame of the given GIF
				for (i = 0; i < frameCount; i++)
				{
					GIFImage.SelectActiveFrame(dimension, i);
					Bitmap frame = new Bitmap(GIFImage.Width, GIFImage.Height);

					System.Drawing.Graphics.FromImage(frame).DrawImage(GIFImage, Point.Empty);

					Texture2D frameTexture = new Texture2D(frame.Width, frame.Height);

					for (x = 0; x < frame.Width; x++)
					{
						for (y = 0; y < frame.Height; y++)
						{
							System.Drawing.Color sourceColor = frame.GetPixel(x, y);
							frameTexture.SetPixel
							(
								x,
								frame.Height - 1 - y,
								new Color32
								(
									sourceColor.R,
									sourceColor.G,
									sourceColor.B,
									sourceColor.A
								)
							);
						}
					}

					frameTexture.Apply();

					GIFFrames.Add(frameTexture);
				}
			}

			return GIFFrames;
		}

	#endregion


	#region Accessors

		/// <summary>
		/// Get the last selected path from the Editor Prefs.
		/// </summary>
		private static string GetGIFPath()
		{
			return EditorPrefs.GetString(c_GIFPathPrefKey, Application.dataPath);
		}
		
		/// <summary>
		/// Get the last given Images Base Name from the Editor Prefs.
		/// </summary>
		private static string GetImagesBaseName()
		{
			return EditorPrefs.GetString(c_ImagesBaseNamePrefKey, Application.dataPath);
		}

		/// <summary>
		/// Get the last selected target path from the Editor Prefs.
		/// </summary>
		private static string GetTargetFolderPath()
		{
			return EditorPrefs.GetString(c_TargetPathPrefKey, Application.dataPath);
		}

		/// <summary>
		/// Get the complete path for a given frame.
		/// </summary>
		/// <param name="_Extension">The extension without "."</param>
		private static string GetPathForFrame(int _FrameNumber, string _Extension)
		{
			string frameNumber = string.Empty;

			if(_FrameNumber < 100)
			{
				frameNumber += "0";

				if(_FrameNumber < 10)
				{
					frameNumber += "0";
				}
			}

			string	path =	Application.dataPath;
					path += "/" + GetTargetFolderPath();
					path += "/" + GetImagesBaseName();
					path +=	frameNumber + _FrameNumber;
					path += "." + _Extension;


			return path;
		}

	#endregion

}

}