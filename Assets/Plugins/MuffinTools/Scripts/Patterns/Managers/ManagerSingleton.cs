﻿using System;
using System.Collections.Generic;

namespace MuffinTools
{

/// <summary>
/// 
/// 
/// 
/// </summary>
/// 
/// <typeparam name="T">The Singleton's type.</typeparam>
/// <typeparam name="U">The managed elements' type.</typeparam>

public class ManagerSingleton<T, U> : Manager<U>
	where T : class
{


	#region Attributes

		// Resources

			// Singleton

		// Contains the only instance of the class.
		protected	static T		s_Instance	= null;

		private		static object	s_InitLock	= new object();

	#endregion


	#region Initialisation

		protected ManagerSingleton()
		{

		}

		/// <summary>
		/// Create a thread-safe instance and set it as this class' Instane.
		/// </summary>
		private static void CreateInstance()
		{
			lock (s_InitLock)
			{
				if (s_Instance == null)
				{
					Type t = typeof(T);

					System.Reflection.ConstructorInfo[] constructors = t.GetConstructors();

					if (constructors.Length > 0)
					{
						throw new InvalidOperationException("Error : A class using Singleton must not have any public constructor.");
					}

					s_Instance = (T)Activator.CreateInstance(t, true);
				}
			}
		}

	#endregion


	#region Accessors

		/// <summary>
		/// Create a (thread-safe) instance and save it if it doesn't exist.
		/// </summary>
		/// <returns>Returns the only instance of the class.</returns>
		public static T Instance
		{
			get
			{
				if (s_Instance == null)
				{
					CreateInstance();
				}

				return s_Instance;
			}
		}

	#endregion

}

}