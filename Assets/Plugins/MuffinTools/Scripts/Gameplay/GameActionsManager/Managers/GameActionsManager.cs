﻿using System.Collections.Generic;

using UnityEngine;

///<summary>
///
///		This component should be placed on a GameObject used for a Character or anything that will
///	do actions in the game.
///		It manages the actions of the element, and its statements.
///		The Actions implements the controller and the logic in a same script (see GameAction class
///	for details).
///	
///		The statements are simple strings that you can add/remove and read from any action. So,
///	they are useful to transmit informations like "isJumping", "isDashing", ... without references
///	to all other actions in themselves.
/// 
///</summary>

namespace MuffinTools
{

[AddComponentMenu("Scripts/Actions/Game Actions Manager")]
public class GameActionsManager : MonoBehaviour
{

	#region Attributes

		// References

		private GameAction[] m_GameActions = { };

		// Flow

		private List<string> m_Statements = new List<string>();

	#endregion


	#region Engine Methods

		/// <summary>
		/// Initializes this ActionsManager's Actions list. Then call GAInit() and GAAwake() on all Actions.
		/// </summary>
		protected virtual void Awake()
		{
			InitActions();

			int count = m_GameActions.Length;
			int i = 0;

			for (; i < count; i++)
			{
				m_GameActions[i].GAInit();
			}

			for(i = 0; i < count; i++)
			{
				m_GameActions[i].GAAwake();
			}
		}

		/// <summary>
		/// Calls GAStart() on all this ActionsManager's Actions.
		/// </summary>
		protected virtual void Start()
		{
			int count = m_GameActions.Length;
			for (int i = 0; i < count; i++)
			{
				m_GameActions[i].GAStart();
			}
		}

		/// <summary>
		/// Calls GAUpdate() on all this ActionsManager's Actions.
		/// </summary>
		protected virtual void Update()
		{
			int count = m_GameActions.Length;
			for (int i = 0; i < count; i++)
			{
				m_GameActions[i].GAUpdate();
			}
		}

		/// <summary>
		/// Calls GAFixedUpdate() on all this ActionsManager's Actions.
		/// </summary>
		protected virtual void FixedUpdate()
		{
			int count = m_GameActions.Length;
			for (int i = 0; i < count; i++)
			{
				m_GameActions[i].GAFixedUpdate();
			}
		}

		/// <summary>
		/// Calls GALateUpdate() on all this ActionsManager's Actions.
		/// </summary>
		protected virtual void LateUpdate()
		{
			int count = m_GameActions.Length;
			for (int i = 0; i < count; i++)
			{
				m_GameActions[i].GALateUpdate();
			}
		}

		/// <summary>
		/// Calls GADestroy() on all this ActionsManager's Actions.
		/// </summary>
		protected virtual void OnDestroy()
		{
			int count = m_GameActions.Length;
			for (int i = 0; i < count; i++)
			{
				m_GameActions[i].GADestroy();
			}
		}

	#endregion


	#region Public Methods

		/// <summary>
		/// Adds the given statement in the list if it's not empty and not already in.
		/// </summary>
		/// <returns>Returns true if the operation is successful, false if not.</returns>
		public bool AddStatement(string _Statement)
		{
			if(_Statement != string.Empty && !m_Statements.Contains(_Statement))
			{
				m_Statements.Add(_Statement);
				return true;
			}
			return false;
		}

		/// <summary>
		/// Removes the given statement in the list if it's not empty and not already in.
		/// </summary>
		/// <returns>Returns true if the operation is successful, false if not.</returns>
		public bool RemoveStatement(string _Statement)
		{
			if(_Statement != string.Empty)
			{
				int index = m_Statements.IndexOf(_Statement);
				if(index != -1)
				{
					m_Statements.RemoveAt(index);
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Clear the statements list.
		/// </summary>
		/// <returns>Returns true if the operation is successful, false if not.</returns>
		public bool ClearStatements()
		{
			m_Statements.Clear();
			return true;
		}

		/// <summary>
		/// Checks if the named statement is in this ActionsManager's statements list.
		/// </summary>
		public bool HasStatement(string _Statement)
		{
			return
			(
				_Statement != string.Empty &&
				m_Statements.IndexOf(_Statement) != -1
			);
		}

	#endregion


	#region Protected Methods
	#endregion


	#region Private Methods

		private void InitActions()
		{
			GameAction[] actionsInChildren = GetComponentsInChildren<GameAction>();
			List<GameAction> assignedActions = new List<GameAction>();

			int count = actionsInChildren.Length;
			for (int i = 0; i < count; i++)
			{
				if(actionsInChildren[i].AssignManager(this))
				{
					assignedActions.Add(actionsInChildren[i]);
				}
			}

			m_GameActions = assignedActions.ToArray();
		}

	#endregion


	#region Accessors

		protected GameAction[] Actions
		{
			get { return m_GameActions; }
		}

        protected List<string> Statements
        {
            get { return m_Statements; }
        }

	#endregion


	#region Debug & Tests

		#if UNITY_EDITOR
		#endif

	#endregion

}

}