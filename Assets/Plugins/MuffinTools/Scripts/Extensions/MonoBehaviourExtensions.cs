﻿using UnityEngine;

namespace MuffinTools
{

///<summary>
/// 
///</summary>
public static class MonoBehaviourExtensions
{

	#region Public Methods

		/// <summary>
		/// Stop the given Coroutine if it's not null and set
		/// it to null.
		/// </summary>
		public static void CancelCoroutine(this MonoBehaviour _MonoBehaviour, ref Coroutine _Coroutine)
		{
			if(_Coroutine != null)
			{
				_MonoBehaviour.StopCoroutine(_Coroutine);
				_Coroutine = null;
			}
		}

	#endregion

}

}