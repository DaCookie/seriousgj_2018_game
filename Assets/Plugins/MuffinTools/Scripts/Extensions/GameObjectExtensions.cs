﻿using UnityEngine;

using System.Collections.Generic;

namespace MuffinTools
{

///<summary>
///
///		This class contains some extensions for GameObjects.
///		To use a GameObject extension, use gameObject.[MethodName]().
/// 
///</summary>

public static class GameObjectExtensions
{

	#region Public Methods

		/// <summary>
		/// Try to get a component of the given type on the current object.
		/// If no component has been found, add the required component on the current object.
		/// </summary>
		/// <typeparam name="T">The type of the required component.</typeparam>
		/// <returns>Returns the found component or the just-created one.</returns>
		public static T RequireComponent<T>(this GameObject _Obj)
			where T : Component
		{
			T comp = _Obj.GetComponent<T>();

			return (comp != null) ? comp : _Obj.AddComponent<T>();
		}

		/// <summary>
		/// Use GetComponentInChildren() from the root of the current object.
		/// </summary>
		/// <returns>Returns the found component.</returns>
		public static T GetComponentFromRoot<T>(this GameObject _Obj)
			where T : Component
		{
			return _Obj.transform.root.GetComponentInChildren<T>();
		}

		/// <summary>
		/// Use GetComponentsInChildren() from the root of the current object.
		/// </summary>
		/// <returns>Returns the found components.</returns>
		public static T[] GetComponentsFromRoot<T>(this GameObject _Obj)
		{
			return _Obj.transform.root.GetComponentsInChildren<T>();
		}

		/// <summary>
		/// Get components using the required method.
		/// </summary>
		/// <returns>Returns the found components.</returns>
		public static T[] GetComponents<T>(this GameObject _Obj, GetComponentsMethod _Method)
			where T : Component
		{
			T[] components = null;

			switch(_Method)
			{
				case GetComponentsMethod.GetComponents:
					components = _Obj.GetComponents<T>();
					break;

				case GetComponentsMethod.GetComponentsInChildren:
					components = _Obj.GetComponentsInChildren<T>();
					break;

				case GetComponentsMethod.GetComponentsFromRoot:
					components = _Obj.GetComponentsFromRoot<T>();
					break;

				default:
					components = new T[] { };
					break;
			}

			return components;
		}

		/// <summary>
		/// Gets the size of the given object, based on its renderer or its collider.
		/// </summary>
		/// <returns>Returns the calculated extents, or Vector3.one if the GameObject has
		/// no Renderer or Collider component.</returns>
		public static Vector3 GetExtents(this GameObject _Obj)
		{
			Renderer renderer = _Obj.GetComponent<Renderer>();
			if (renderer != null)
			{
				return renderer.bounds.extents;
			}

			Collider collider = _Obj.GetComponent<Collider>();
			if (collider != null)
			{
				return renderer.bounds.extents;
			}

			return Vector3.one;
		}

	#endregion

}

}