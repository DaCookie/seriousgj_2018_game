﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

///<summary>
/// 
///		
/// 
///</summary>

public enum AnimCurveColor
{
	Black,
	Blue,
	Cyan,
	Gray,
	Green,
	Grey,
	Magenta,
	Red,
	White,
	Yellow
}

public class AnimCurveAttribute : PropertyAttribute
{

	#region Members

		private float m_MinTime = 0.0f;
		private float m_MaxTime = 1.0f;

		private float m_MinValue = 0.0f;
		private float m_MaxValue = 1.0f;

		private Color m_CurveColor = Color.green;

	#endregion


	#region Initialisation / Destruction

		public AnimCurveAttribute()
		{
			
		}

		public AnimCurveAttribute(AnimCurveColor _CurveColor)
		{
			m_CurveColor = GetCurveColor(_CurveColor);
		}

		public AnimCurveAttribute(float _MaxTime, float _MaxValue)
		{
			m_MaxTime = _MaxTime;
			m_MaxValue = _MaxValue;
		}

		public AnimCurveAttribute(float _MaxTime, float _MaxValue, AnimCurveColor _CurveColor)
		{
			m_MaxTime = _MaxTime;
			m_MaxValue = _MaxValue;

			m_CurveColor = GetCurveColor(_CurveColor);
		}

		public AnimCurveAttribute(float _MinTime, float _MaxTime, float _MinValue, float _MaxValue)
		{
			m_MinTime = _MinTime;
			m_MinValue = _MinValue;

			m_MaxTime = _MaxTime;
			m_MaxValue = _MaxValue;
		}

		public AnimCurveAttribute(float _MinTime, float _MaxTime, float _MinValue, float _MaxValue, AnimCurveColor _CurveColor)
		{
			m_MinTime = _MinTime;
			m_MinValue = _MinValue;

			m_MaxTime = _MaxTime;
			m_MaxValue = _MaxValue;

			m_CurveColor = GetCurveColor(_CurveColor);
		}

		public AnimCurveAttribute(float _MinTime, float _MaxTime, float _MinValue, float _MaxValue, float _ColorR, float _ColorG, float _ColorB)
		{
			m_MinTime = _MinTime;
			m_MinValue = _MinValue;

			m_MaxTime = _MaxTime;
			m_MaxValue = _MaxValue;

			m_CurveColor = new Color(_ColorR, _ColorG, _ColorB);
		}

	#endregion


	#region Accessors

		public float MinTime
		{
			get { return m_MinTime; }
		}

		public float MaxTime
		{
			get { return m_MaxTime; }
		}

		public float MinValue
		{
			get { return m_MinValue; }
		}

		public float MaxValue
		{
			get { return m_MaxValue; }
		}

		public Color CurveColor
		{
			get { return m_CurveColor; }
		}

		private Color GetCurveColor(AnimCurveColor _Color)
		{
			Color color = Color.white;
			switch(_Color)
			{
				case AnimCurveColor.Black:
					color = Color.black;
					break;
				case AnimCurveColor.Blue:
					color = Color.blue;
					break;
				case AnimCurveColor.Cyan:
					color = Color.cyan;
					break;
				case AnimCurveColor.Gray:
					color = Color.grey;
					break;
				case AnimCurveColor.Green:
					color = Color.green;
					break;
				case AnimCurveColor.Grey:
					color = Color.grey;
					break;
				case AnimCurveColor.Magenta:
					color = Color.magenta;
					break;
				case AnimCurveColor.Red:
					color = Color.red;
					break;
				case AnimCurveColor.Yellow:
					color = Color.yellow;
					break;
			}
			return color;
		}

		public Rect GetRanges()
		{
			return new Rect(m_MinTime, m_MinValue, m_MaxTime, m_MaxValue);
		}

	#endregion

}

#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(AnimCurveAttribute))]
public class AnimCurveDrawer : PropertyDrawer
{

	#region UI

		public override void OnGUI(Rect _Position, SerializedProperty _Property, GUIContent _Label)
		{
			AnimCurveAttribute animCurve = attribute as AnimCurveAttribute;

			if (_Property.propertyType == SerializedPropertyType.AnimationCurve)
			{
				EditorGUI.CurveField(_Position, _Property, animCurve.CurveColor, animCurve.GetRanges());
				_Property.serializedObject.ApplyModifiedProperties();
			}
			else
			{
				EditorGUI.HelpBox(_Position, "The AnimCurve attribute can only be used on AnimationCurve properties.", MessageType.None);
			}
		}

	#endregion

}

#endif