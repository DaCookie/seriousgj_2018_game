﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using System.Collections.Generic;
#endif

///<summary>
/// 
///		Make a field share a total value with other fields ("dependencies").
///		NOTE : This attribute only works with float fields.
/// 
///</summary>

public class DependencyRangeAttribute : PropertyAttribute
{

	#region Members

		private float m_Total = 0.0f;
		private string m_TotalFieldName = string.Empty;
		private string[] m_Dependencies = { };
		private bool m_CanBeSmallerThanTotal = false;
		private bool m_EnableDebug = false;

	#endregion


	#region Initialisation / Destruction

		/// <summary>
		/// Make this field's value depending on other values.
		/// </summary>
		/// <param name="_Total">The total value shared between fields.</param>
		/// <param name="_Dependencies">Names of the variables that this field depends.</param>
		/// <param name="_CanBeSmallerThanTotal">Defines if the total amout of the field and its
		/// dependencies can be smaller than the total value.</param>
		public DependencyRangeAttribute(float _Total, string[] _Dependencies, bool _CanBeSmallerThanTotal = false, bool _EnableDebug = false)
		{
			m_Total					= _Total;
			m_Dependencies			= _Dependencies;
			m_CanBeSmallerThanTotal = _CanBeSmallerThanTotal;
			m_EnableDebug			= _EnableDebug;
		}

		/// <summary>
		/// Make this field's value depending on other values.
		/// </summary>
		/// <param name="_TotalFieldName">The name of the field that represents the total value
		/// shared between fields.</param>
		/// <param name="_Dependencies">Names of the variables that this field depends.</param>
		/// <param name="_CanBeSmallerThanTotal">Defines if the total amout of the field and its
		/// dependencies can be smaller than the total value.</param>
		public DependencyRangeAttribute(string _TotalFieldName, string[] _Dependencies, bool _CanBeSmallerThanTotal = false, bool _EnableDebug = false)
		{
			m_TotalFieldName		= _TotalFieldName;
			m_Dependencies			= _Dependencies;
			m_CanBeSmallerThanTotal = _CanBeSmallerThanTotal;
			m_EnableDebug			= _EnableDebug;
		}

	#endregion


	#region Accessors

		public float Total
		{
			get { return m_Total; }
		}

		public string TotalFieldName
		{
			get { return m_TotalFieldName; }
		}

		public string[] Dependencies
		{
			get { return m_Dependencies; }
		}

		public bool CanBeSmallerThanTotal
		{
			get { return m_CanBeSmallerThanTotal; }
		}

		public bool EnableDebug
		{
			get { return m_EnableDebug; }
		}

	#endregion

}

#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(DependencyRangeAttribute))]
public class DependencyRangeDrawer : PropertyDrawer
{

	#region Attributes

		// Constants

		private const float c_MinValue = 0;
		private const float c_ValueThreshold = 0.0001f;

	#endregion


	#region UI

		public override void OnGUI(Rect _Position, SerializedProperty _Property, GUIContent _Label)
		{
			DependencyRangeAttribute attr = attribute as DependencyRangeAttribute;

			float maxSharedValue = GetTotal(attr, _Property.serializedObject);
			SerializedProperty[] dependencies = GetDependencies(attr, _Property);

			EditorGUI.Slider(_Position, _Property, 0, maxSharedValue);

			// Calculate the total amount of the property ans its dependencies
			float total = _Property.floatValue;
			int count = dependencies.Length;
			for(int i = 0; i < count; i++)
			{
				if(dependencies[i].floatValue > 0 && dependencies[i].floatValue <= Mathf.Epsilon)
				{
					dependencies[i].floatValue = 0.0f;
				}

				total += dependencies[i].floatValue;
			}

			// If the total is higher than the maximum value
			if(total > maxSharedValue)
			{
				float diff = (total - maxSharedValue) / count;
				for(int i = 0; i < count; i++)
				{
					dependencies[i].floatValue -= diff;
					// If the new value is smaller than the minimal value
					if (dependencies[i].floatValue <= c_MinValue + c_ValueThreshold)
					{
						dependencies[i].floatValue = c_MinValue;
					}
				}
			}

			// If the total is smaller than the maximum value but we don't tolerate it
			else if(total < maxSharedValue && !attr.CanBeSmallerThanTotal)
			{
				float diff = (maxSharedValue - total) / count;
				for (int i = 0; i < count; i++)
				{
					dependencies[i].floatValue += diff;
					// If the new value is higher than the minimal value
					if (dependencies[i].floatValue > maxSharedValue)
					{
						dependencies[i].floatValue = maxSharedValue;
					}
				}
			}

			_Property.serializedObject.ApplyModifiedProperties();
		}

	#endregion

	
	#region Accessors

		/// <summary>
		/// Gets the named dependency fields as SerializedProperty.
		/// </summary>
		private SerializedProperty[] GetDependencies(DependencyRangeAttribute _Attribute, SerializedProperty _Property)
		{
			List<SerializedProperty> serializedDependencies = new List<SerializedProperty>();

			int count = _Attribute.Dependencies.Length;
			for(int i = 0; i < count; i++)
			{
				if(_Property.name == _Attribute.Dependencies[i])
				{
					continue;
				}

				SerializedProperty serializedDependency = _Property.serializedObject.FindProperty(_Attribute.Dependencies[i]);
				if (serializedDependency != null)
				{
					serializedDependencies.Add(serializedDependency);
				}

				else if (_Attribute.EnableDebug)
				{
					Debug.LogWarning("ERROR from DependencyRange attribute : The field \"" + _Attribute.Dependencies[i] + "\" doesn't exist on this object (\"" + _Property.serializedObject.context.name + "\").");
				}
			}

			return serializedDependencies.ToArray();
		}

		/// <summary>
		/// Gets the total given value for the attribute.
		/// </summary>
		/// <returns>Returns the total field if gave and valid, or the total value of the attribute.</returns>
		private float GetTotal(DependencyRangeAttribute _Attribute, SerializedObject _Object)
		{
			if(!string.IsNullOrEmpty(_Attribute.TotalFieldName))
			{
				SerializedProperty totalProperty = _Object.FindProperty(_Attribute.TotalFieldName);
				if(totalProperty != null)
				{
					return totalProperty.floatValue;
				}

				else if(_Attribute.EnableDebug)
				{
					Debug.LogWarning("ERROR from DependencyRange attribute : The field \"" + _Attribute.TotalFieldName + "\" doesn't exist on this object (\"" + _Object.context.name + "\").");
				}
			}

			return _Attribute.Total;
		}

	#endregion

}

#endif