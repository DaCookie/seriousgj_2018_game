﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

///<summary>
/// 
///		Indent the following member by the given indent
///	level in the inspector.
/// 
///</summary>

public class IndentAttribute : PropertyAttribute
{

	#region Members

		private int m_IndentLevel = 1;

	#endregion


	#region Initialisation / Destruction

		public IndentAttribute()
		{
			
		}

		public IndentAttribute(int _IndentLevel)
		{
			m_IndentLevel = _IndentLevel;
		}

	#endregion


	#region Accessors

		public int IndentLevel
		{
			get { return m_IndentLevel; }
		}

	#endregion

}

#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(IndentAttribute))]
public class IndentDrawer : PropertyDrawer
{

	#region UI

		public override void OnGUI(Rect _Position, SerializedProperty _Property, GUIContent _Label)
		{
			IndentAttribute indentAttribute = attribute as IndentAttribute;

			int lastIndentLevel = EditorGUI.indentLevel;

			EditorGUI.indentLevel = indentAttribute.IndentLevel;
			EditorGUI.PropertyField(_Position, _Property);
			EditorGUI.indentLevel = lastIndentLevel;
		}

	#endregion

}

#endif