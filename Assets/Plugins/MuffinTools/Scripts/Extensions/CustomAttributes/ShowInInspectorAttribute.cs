﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

///<summary>
/// 
///		This attribute allow you to show a serialized member
///	in the inspector, but it can't be edited.
/// 
///</summary>

public class ReadonlyAttribute : PropertyAttribute
{

	

}

#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(ReadonlyAttribute))]
public class ShowInInspectorDrawer : PropertyDrawer
{

	#region UI

		public override void OnGUI(Rect _Position, SerializedProperty _Property, GUIContent _Label)
		{
			GUI.enabled = false;
			EditorGUI.PropertyField(_Position, _Property);
			GUI.enabled = true;
		}

	#endregion

}

#endif