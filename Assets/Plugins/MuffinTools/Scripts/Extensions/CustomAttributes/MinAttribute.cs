﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

///<summary>
/// 
///		The MinAttribute block a value at a given number
///	if the value goes under that given number. The attribute
///	can be used on :
///		- Float
///		- Integer
///		- Vector2
///		- Vector3
/// 
///</summary>

public class MinAttribute : PropertyAttribute
{

	#region Members

		private float m_Min = 0.0f;

	#endregion


	#region Initialisation / Destruction

		private MinAttribute()
		{

		}

		public MinAttribute(float _Min)
		{
			m_Min = _Min;
		}

		public MinAttribute(int _Min)
		{
			m_Min = _Min;
		}

	#endregion


	#region Accessors

		public float Min
		{
			get { return m_Min; }
		}

	#endregion

}

#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(MinAttribute))]
public class MinDrawer : PropertyDrawer
{

	#region UI

		public override void OnGUI(Rect _Position, SerializedProperty _Property, GUIContent _Label)
		{
			MinAttribute	minAttribute	= attribute as MinAttribute;
			float			min				= minAttribute.Min;

			switch(_Property.propertyType)
			{
				case SerializedPropertyType.Float :
					_Property.floatValue = GetValue(min, _Property.floatValue);
					break;

				case SerializedPropertyType.Integer :
					_Property.intValue = GetValue(min, _Property.intValue);
					break;

				case SerializedPropertyType.Vector2 :
					_Property.vector2Value = GetValue(min, _Property.vector2Value);
					break;

				case SerializedPropertyType.Vector3 :
					_Property.vector3Value = GetValue(min, _Property.vector3Value);
					break;

				default :
					EditorGUI.HelpBox(_Position, "The Min attribute can't be use for this property type.", MessageType.None);
					break;
			}

			EditorGUI.PropertyField(_Position, _Property);
		}

	#endregion

	
	#region Accessors

		private int GetValue(float _Min, int _Value)
		{
			return System.Convert.ToInt32(GetClampedValue(_Min, _Value));
		}

		private float GetValue(float _Min, float _Value)
		{
			return GetClampedValue(_Min, _Value);
		}

		private Vector2 GetValue(float _Min, Vector2 _Value)
		{
			_Value.x = GetClampedValue(_Min, _Value.x);
			_Value.y = GetClampedValue(_Min, _Value.y);

			return _Value;
		}

		private Vector3 GetValue(float _Min, Vector3 _Value)
		{
			_Value.x = GetClampedValue(_Min, _Value.x);
			_Value.y = GetClampedValue(_Min, _Value.y);
			_Value.z = GetClampedValue(_Min, _Value.z);

			return _Value;
		}

		private float GetClampedValue(float _Min, float _Value)
		{
			_Value = (_Value < _Min) ? _Min : _Value;

			return _Value;
		}

	#endregion

}

#endif