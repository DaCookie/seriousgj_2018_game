﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

///<summary>
/// 
///		This attribute draws a MinMaxSlider field in editor.
///		Since this type of field is used to set 2 values, this attribute
///	can only be applied on Vector2 properties.
///		As output, the X value of Vector2 property is "min", and Y
///	value is "max".
/// 
///</summary>

public class RangeMinMaxAttribute : PropertyAttribute
{

	#region Members

		private float m_MinLimit = 0.0f;
		private float m_MaxLimit = 0.0f;

		private bool m_ForceInt = false;

	#endregion


	#region Initialisation / Destruction

		public RangeMinMaxAttribute(float _MinLimit, float _MaxLimit, bool _ForceIntegerValues)
		{
			m_MinLimit = _MinLimit;
			m_MaxLimit = _MaxLimit;
			m_ForceInt = _ForceIntegerValues;
		}

		public RangeMinMaxAttribute(int _MinLimit, int _MaxLimit, bool _ForceIntegerValues = true)
		{
			m_MinLimit = (float)_MinLimit;
			m_MaxLimit = (float)_MaxLimit;
			m_ForceInt = _ForceIntegerValues;
		}

	#endregion


	#region Accessors

		public float MinLimit
		{
			get { return m_MinLimit; }
		}

		public float MaxLimit
		{
			get { return m_MaxLimit; }
		}

		public bool ForceInt
		{
			get { return m_ForceInt; }
		}

	#endregion

}

#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(RangeMinMaxAttribute))]
public class RangeMinMaxDrawer : PropertyDrawer
{

	#region Attributes

		private const float c_NumberFieldWidth = 45.0f;
		private const float c_Margin = 4.0f;

	#endregion


	#region UI

		public override void OnGUI(Rect _Position, SerializedProperty _Property, GUIContent _Label)
		{
			RangeMinMaxAttribute attr = attribute as RangeMinMaxAttribute;

			float min = _Property.vector2Value.x;
			float max = _Property.vector2Value.y;

			Vector2 position = _Position.position;
			Vector2 size = _Position.size;

			// Draw label
			size.x = EditorGUIUtility.labelWidth;
			EditorGUI.LabelField(new Rect(position, size), _Label);
			
			// Draw "min" number field
			position.x += size.x;
			size.x = c_NumberFieldWidth;
			min = Mathf.Clamp(EditorGUI.FloatField(new Rect(position, size), min), attr.MinLimit, max);

			// Draw slider
			position.x += size.x + c_Margin;
			size.x = _Position.size.x - (EditorGUIUtility.labelWidth + (c_NumberFieldWidth + c_Margin) * 2);
			EditorGUI.MinMaxSlider(new Rect(position, size), ref min, ref max, attr.MinLimit, attr.MaxLimit);

			// Draw "max" number field
			position.x += size.x + c_Margin;
			size.x = c_NumberFieldWidth;
			max = Mathf.Clamp(EditorGUI.FloatField(new Rect(position, size), max), min, attr.MaxLimit);

			if(attr.ForceInt)
			{
				min = Mathf.Floor(min);
				max = Mathf.Floor(max);
			}

			_Property.vector2Value = new Vector2(min, max);
		}

	#endregion

	
	#region Accessors
	#endregion

}

#endif