﻿namespace MuffinTools
{

	///<summary>
	/// 
	///		This file contains a bunch of enumerations that can
	/// be useful.
	/// 
	///</summary>

	public enum UpdateType
	{
		Update,
		FixedUpdate,
		LateUpdate
	}

	public enum GetComponentsMethod
	{
		None,
		GetComponents,
		GetComponentsInChildren,
		GetComponentsFromRoot
	}

}