﻿using UnityEngine;
using UnityEngine.UI;

namespace MuffinTools
{

///<summary>
/// 
///		
/// 
///</summary>

[AddComponentMenu("MuffinTools/Examples/Fake Loader/FL_UILoading")]
public class FL_UILoading : MonoBehaviour
{

	#region Attributes

		// References

		private Slider m_Slider = null;

	#endregion

	
	#region Engine Methods

		private void Awake()
		{
			m_Slider = GetComponent<Slider>();
		}

		private void Update()
		{
			m_Slider.value = FakeLoader.Instance.LoadingRatio;
		}

	#endregion

	
	#region Public Methods
	#endregion

	
	#region Protected Methods
	#endregion

	
	#region Private Methods
	#endregion

	
	#region Accessors
	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR
		#endif

	#endregion

}

}