﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace MuffinTools
{

///<summary>
/// 
///		
/// 
///</summary>

[AddComponentMenu("MuffinTools/Fancy/Fake Loader")]
public class FakeLoader : MonoSingleton<FakeLoader>
{

	#region Attributes

		// Settings

		[Header("Settings")]

		[SerializeField, Min(-1)]
		private int m_LoadingSceneBuildIndex = -1;

		[SerializeField, Min(0.0f)]
		private float m_DefaultLoadingDuration = 2.0f;

		// Flow

		private int m_TargetScene = -1;

		private float m_LoadingDuration = 0.0f;
		private float m_LoadingTimer = 0.0f;

	#endregion

	
	#region Engine Methods

		private void Update()
		{
			if(IsLoading())
			{
				m_LoadingTimer += Time.deltaTime;
				if(!IsLoading())
				{
					SceneManager.LoadScene(m_TargetScene);
				}
			}
		}

	#endregion

	
	#region Public Methods

		public bool LoadScene(int _SceneID, float _LoadingDuration)
		{
			// If we don't want loading screen, 
			int sceneToLoad = (_LoadingDuration <= 0.0f) ? _SceneID : m_LoadingSceneBuildIndex;

			if (sceneToLoad < 0)
			{
				Debug.LogWarning("ERROR from FakeLoader.LoadScene() : You must set and not load directly the \"loading\" scene (scene to load = " + sceneToLoad + ").");
				return false;
			}

			if (!IsSceneInRange(sceneToLoad))
			{
				Debug.LogError("ERROR from FakeLoader.LoadScene() : The scene n°" + sceneToLoad + " is out of Build Settings scenes array range.");
				return false;
			}

			m_TargetScene = _SceneID;
			m_LoadingDuration = _LoadingDuration;

			if(m_LoadingDuration > 0.0f)
			{
				SceneManager.LoadScene(m_LoadingSceneBuildIndex);
				m_LoadingTimer = 0.0f;
			}
			else
			{
				SceneManager.LoadScene(sceneToLoad);
				m_LoadingTimer = m_LoadingDuration;
			}

			return true;
		}

		public bool LoadScene(int _SceneID)
		{
			return LoadScene(_SceneID, m_DefaultLoadingDuration);
		}

		public bool ReloadScene()
		{
			return LoadScene(SceneManager.GetActiveScene().buildIndex);
		}

		public bool ReloadScene(float _LoadingDuration)
		{
			return LoadScene(SceneManager.GetActiveScene().buildIndex, _LoadingDuration);
		}

	#endregion

	
	#region Protected Methods

		protected override void OnInstanceInit()
		{
			DontDestroyOnLoad(this);
		}

		protected override void OnDestroyMessage()
		{
			Destroy(gameObject);
		}

	#endregion

	
	#region Private Methods
	#endregion

	
	#region Accessors

		private bool IsLoading()
		{
			return (m_LoadingTimer < m_LoadingDuration);
		}

		private bool IsSceneInRange(int _SceneBuildIndex)
		{
			int count = SceneManager.sceneCountInBuildSettings;
			return (_SceneBuildIndex >= 0 && _SceneBuildIndex < count);
		}

		public float LoadingRatio
		{
			get { return (m_LoadingDuration > 0.0f) ? Mathf.Clamp01(m_LoadingTimer / m_LoadingDuration) : 0.0f; }
		}

		public float LoadingDuration
		{
			get { return m_LoadingDuration; }
		}

		public float LoadingTimer
		{
			get { return m_LoadingTimer; }
		}

	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR
		#endif

	#endregion

}

}