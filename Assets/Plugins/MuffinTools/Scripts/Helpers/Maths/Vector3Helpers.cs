﻿using UnityEngine;

namespace MuffinTools
{

///<summary>
/// 
///</summary>
public static class Vector3Helpers
{

	#region Miscellaneous

		/// <summary>
		/// If the given Vector's magnitude is less than the MinMagnitude, clamp
		/// its magnitude to MinMagnitude.
		/// Same for the MaxMagnitude value.
		/// 
		/// IMPORTANT : The magnitudes are squared before being compared. So if
		/// you want this method to work, you must give positive values for both
		/// Min and Max Magnitudes.
		/// </summary>
		/// <param name="_MinMagnitude">The minimum allowed magnitude (must be
		/// more than 0 to work).</param>
		/// <param name="_MaxMagnitude">The maximum allowed magnitude (must be
		/// more than 0 to work).</param>
		public static void ClampMagnitude(ref Vector3 _Vector, float _MinMagnitude, float _MaxMagnitude)
		{
			float sqrMagnitude = _Vector.sqrMagnitude;

			_MinMagnitude = Mathf.Abs(_MinMagnitude);

			if (sqrMagnitude < _MinMagnitude * _MinMagnitude)
			{
				_Vector = _Vector.normalized * _MinMagnitude;
				return;
			}

			_MaxMagnitude = Mathf.Abs(_MaxMagnitude);

			if (sqrMagnitude > _MaxMagnitude * _MaxMagnitude)
			{
				_Vector = _Vector.normalized * _MaxMagnitude;
			}
		}

		/// <summary>
		/// Check if the distance between the given Origin and the given Target
		/// is less than the given Range.
		/// </summary>
		public static bool IsInRange(Vector3 _From, Vector3 _Target, float _Range)
		{
			return ((_From - _Target).sqrMagnitude < _Range * _Range);
		}

	#endregion


	#region Symmetry & Rotations

		/// <summary>
		/// Apply central symmetry to get a point in space.
		/// </summary>
		/// <param name="_Center">The symmetry center.</param>
		/// <param name="_Direction">The vector that describes the movement of the
		/// point (this vector will rotate, then the point to get will be at the end
		/// of this vector.</param>
		/// <param name="_Angle">The angle of the rotation.</param>
		/// <param name="_RotationAxis">The axis around which to rotate.</param>
		/// <returns>Returns the point at the end of the rotated vector.</returns>
		public static Vector3 GetPointByCentralSymmetry
		(
			Vector3 _Center,
			Vector3 _Direction,
			float _Angle,
			Vector3 _RotationAxis
		)
		{
			Quaternion rotation = Quaternion.AngleAxis(_Angle, _RotationAxis);

			_Direction = rotation * _Direction;

			return _Center + _Direction;
		}

		/// <summary>
		/// Apply central symmetry to get a point in space.
		/// </summary>
		/// <param name="_Center">The symmetry center.</param>
		/// <param name="_Direction">The vector that describes the movement of the
		/// point. If this vector hasn't a length equal to Distance, its magnitude
		/// will be set at the Distance value. Note that if you calculate the exact
		/// length you want before using this method, you can use the other override
		/// of this method that doesn't need a Distance parameter.</param>
		/// <param name="_Distance">The distance between the symmetry Center and the
		/// point to get.</param>
		/// <param name="_Angle">The angle of the rotation.</param>
		/// <param name="_RotationAxis">The axis around which to rotate.</param>
		/// <returns>Returns the point at the end of the rotated vector.</returns>
		public static Vector3 GetPointByCentralSymmetry
		(
			Vector3 _Center,
			Vector3 _Direction,
			float _Distance,
			float _Angle,
			Vector3 _RotationAxis
		)
		{
			// If Base Vector's magnitude is not already the required one and is not normalized, set its magnitude to Distance.
			if (_Direction.sqrMagnitude != _Distance * _Distance)
			{
				if (_Direction.sqrMagnitude != 1.0f)
				{
					_Direction.Normalize();
				}

				_Direction *= _Distance;
			}

			return GetPointByCentralSymmetry(_Center, _Direction, _Angle, _RotationAxis);
		}

		/// <summary>
		/// Make the given vector rotate of a given angle around a given axis.
		/// </summary>
		/// <param name="_Vector">The vector on which to apply the rotation.</param>
		/// <param name="_Angle">The rotation angle.</param>
		/// <param name="_RotationAxis">The vector around which to rotate.</param>
		public static void RotateVector(ref Vector3 _Vector, float _Angle, Vector3 _RotationAxis)
		{
			Quaternion rotation = Quaternion.AngleAxis(_Angle, _RotationAxis);

			_Vector = rotation * _Vector;
		}

	#endregion


	#region Barycentre

		/*
         *  				   ->		    ->           ->
		 *		->	   weightA*OA + weightB*OB + weightC*OC
		 *		OG = ________________________________________
		 *                 weightA + weghtB + weightC
		 */

		/// <summary>
		/// Get the barycentre between all the given elements.
		/// 
		/// The elements are all supposed with a weight of 1. If you want to define
		/// a weight for the elements, inherit objcts from IBarycentreElement and
		/// use the overload FindBarycentre(IBarycentreElement[]).
		/// </summary>
		public static Vector3 FindBarycentre(Vector3[] _Elements)
		{
			int count = _Elements.Length;

			if(count > 0)
			{
				int i = 0;

				Vector3 pointsSum = Vector3.zero;

				for (; i < count; i++)
				{
					pointsSum += _Elements[i];
				}

				return (pointsSum / count);
			}

			return Vector3.zero;
		}

		/// <summary>
		/// Get the barycentre between all the given elements.
		/// 
		/// The elements are all supposed with a weight of 1. If you want to define
		/// a weight for the elements, inherit objects from IBarycentreElement and
		/// use the overload FindBarycentre(IBarycentreElement[]).
		/// </summary>
		public static Vector3 FindBarycentre(System.Collections.Generic.List<Vector3> _Elements)
		{
			int count = _Elements.Count;

			if (count > 0)
			{
				int i = 0;

				Vector3 pointsSum = Vector3.zero;

				for (; i < count; i++)
				{
					pointsSum += _Elements[i];
				}

				return (pointsSum / count);
			}

			return Vector3.zero;
		}

		/// <summary>
		/// Get the barycentre between all the given elements.
		/// 
		/// The elements are all supposed with a weight of 1. If you want to define
		/// a weight for the elements, inherit objcts from IBarycentreElement and
		/// use the overload FindBarycentre(IBarycentreElement[]).
		/// </summary>
		public static Vector3 FindBarycentre(Component[] _Elements)
		{
			int count = _Elements.Length;

			if(count > 0)
			{
				int i = 0;

				Vector3 pointsSum = Vector3.zero;

				for (; i < count; i++)
				{
					pointsSum += _Elements[i].transform.position;
				}

				return (pointsSum / count);
			}

			return Vector3.zero;
		}

		/// <summary>
		/// Get the barycentre between all the given elements.
		/// 
		/// The elements are all supposed with a weight of 1. If you want to define
		/// a weight for the elements, inherit objects from IBarycentreElement and
		/// use the overload FindBarycentre(IBarycentreElement[]).
		/// </summary>
		public static Vector3 FindBarycentre(System.Collections.Generic.List<Component> _Elements)
		{
			int count = _Elements.Count;

			if (count > 0)
			{
				int i = 0;

				Vector3 pointsSum = Vector3.zero;

				for (; i < count; i++)
				{
					pointsSum += _Elements[i].transform.position;
				}

				return (pointsSum / count);
			}

			return Vector3.zero;
		}

		/// <summary>
		/// Get the barycentre between all the given elements, using their weight.
		/// </summary>
		public static Vector3 FindBarycentre(IBarycentreElement[] _Elements)
		{
			int count = _Elements.Length;
			int i = 0;

			Vector3 pointsSum = Vector3.zero;
			float weightsSum = 0.0f;

			for (; i < count; i++)
			{
				pointsSum	+= _Elements[i].BarycentreWeight * _Elements[i].Position;
				weightsSum	+= _Elements[i].BarycentreWeight;
			}

			return (weightsSum != 0.0f) ? (pointsSum / weightsSum) : Vector3.zero;
		}

		/// <summary>
		/// Get the barycentre between all the given elements, using their weight.
		/// </summary>
		public static Vector3 FindBarycentre(System.Collections.Generic.List<IBarycentreElement> _Elements)
		{
			int count = _Elements.Count;
			int i = 0;

			Vector3 pointsSum = Vector3.zero;
			float weightsSum = 0.0f;

			for (; i < count; i++)
			{
				pointsSum	+= _Elements[i].BarycentreWeight * _Elements[i].Position;
				weightsSum	+= _Elements[i].BarycentreWeight;
			}

			return (weightsSum != 0.0f) ? (pointsSum / weightsSum) : Vector3.zero;
		}

	#endregion

}

}