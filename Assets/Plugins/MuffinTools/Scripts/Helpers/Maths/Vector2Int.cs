﻿using UnityEngine;

namespace MuffinTools
{

public struct Vector2Int
{

	#region Attributes

		// Constants

		public static Vector2Int s_Zero = new Vector2Int(0, 0);
		public static Vector2Int s_Left = new Vector2Int(-1, 0);
		public static Vector2Int s_Right = new Vector2Int(1, 0);
		public static Vector2Int s_Up = new Vector2Int(0, -1);
		public static Vector2Int s_Down = new Vector2Int(0, 1);
		public static Vector2Int s_UpLeft = new Vector2Int(-1, -1);
		public static Vector2Int s_UpRight = new Vector2Int(1, -1);
		public static Vector2Int s_DownLeft = new Vector2Int(-1, 1);
		public static Vector2Int s_DownRight = new Vector2Int(1, 1);

		public static Vector2Int s_One = new Vector2Int(1, 1);

		private int m_X;
		private int m_Y;

	#endregion

	
	#region Initialisation / Destruction

		public Vector2Int(int _BothXAndYValue)
		{
			m_X = _BothXAndYValue;
			m_Y = _BothXAndYValue;
		}

		public Vector2Int(float _BothXAndYValue)
		{
			int value = (int)_BothXAndYValue;

			m_X = value;
			m_Y = value;
		}

		public Vector2Int(int _X, int _Y)
		{
			m_X = _X;
			m_Y = _Y;
		}

		public Vector2Int(float _X, float _Y)
		{
			m_X = (int)_X;
			m_Y = (int)_Y;
		}

		public Vector2Int(Vector2 _Vector)
		{
			m_X = (int)_Vector.x;
			m_Y = (int)_Vector.y;
		}

	#endregion


	#region Public Methods

		public override string ToString()
		{
			return "Vector2Int(" + m_X + "; " + m_Y + ")";
		}

		/// <summary>
		/// Make a dot product between two vectors : (a.x * b.x + a.y * b.y).
		/// 
		/// A dot product returns the cosinus of an angle.
		/// 
		/// If a dot product returns 0, the two vectors have the same direction (Angle = 0°).
		/// If a dot product returns 1, the two vectors are perpendicular (Angle = 90°).
		/// 
		/// NOTE : The dot product works only with normalized vectors.
		/// </summary>
		/// <returns>Returns the result of a dot product between the given vectors.</returns>
		public static float Dot(Vector2Int _Vector1, Vector2Int _Vector2)
		{
			Vector2 vector1 = _Vector1.Normalized;
			Vector2 vector2 = _Vector2.Normalized;

			return ((vector1.x * vector2.x) + (vector1.y * vector2.y));
		}

		/// <summary>
		/// Calculate the angle in degrees between two vectors : (dot(a, b) * 180 / PI).
		/// </summary>
		/// <returns>Returns the angle in degrees between the given vectors.</returns>
		public static float Angle(Vector2Int _Vector1, Vector2Int _Vector2)
		{
			float dot = Dot(_Vector1, _Vector2);

			return (Mathf.Acos(dot) * 180 / Mathf.PI);
		}

	#endregion


	#region Accessors

		/// <returns>
		/// Get the Manhattan distance between two Vector2Int.
		/// </returns>
		private static float GetManhattanDistance(Vector2Int _Vector1, Vector2Int _Vector2)
		{
			int x = Mathf.Abs(_Vector1.X - _Vector2.X);
			int y = Mathf.Abs(_Vector1.Y - _Vector2.Y);

			return x + y;
		}

		/// <returns>
		/// Get the Euclidean distance between two Vector2Int.
		/// </returns>
		private static float GetDistance(Vector2Int _Vector1, Vector2Int _Vector2)
		{
			int x = Mathf.Abs(_Vector1.X - _Vector2.X);
			int y = Mathf.Abs(_Vector1.Y - _Vector2.Y);

			return Mathf.Sqrt(x * x + y * y);
		}

		/// <summary>
		/// Get the magnitude of this vector (sqrt(x * x + y * y)).
		/// </summary>
		public float Magnitude
		{
			get { return Mathf.Sqrt(SqrMagnitude); }
		}

		/// <summary>
		/// Get the square magnitude of this vector (x * x + y * y).
		/// </summary>
		public int SqrMagnitude
		{
			get { return (m_X * m_X + m_Y * m_Y); }
		}

		/// <summary>
		/// Get the normalized vector (x / magnitude, y / magnitude).
		/// </summary>
		public Vector2 Normalized
		{
			get
			{
				float magnitude = Magnitude;

				return new Vector2
				(
					m_X / magnitude,
					m_Y / magnitude
				);
			}
		}

		public int X
		{
			get { return m_X; }
		}

		public int Y
		{
			get { return m_Y; }
		}

	#endregion

	
	#region Operators

		// Equality

		public static bool operator==(Vector2Int _Vector1, Vector2Int _Vector2)
		{
			return (_Vector1.X == _Vector2.X && _Vector1.Y == _Vector2.Y);
		}

		public static bool operator !=(Vector2Int _Vector1, Vector2Int _Vector2)
		{
			return (_Vector1.X != _Vector2.X || _Vector1.Y != _Vector2.Y);
		}

		public override bool Equals(object obj)
		{
			if (!(obj is Vector2Int)) return false;

			return base.Equals(obj) && this == (Vector2Int)obj;
		}

		public override int GetHashCode()
		{
			return m_X ^ m_Y;
		}

		// Additive

		public static Vector2Int operator+(Vector2Int _Vector1, Vector2Int _Vector2)
		{
			return new Vector2Int(_Vector1.X + _Vector2.X, _Vector1.Y + _Vector2.Y);
		}

		public static Vector2Int operator -(Vector2Int _Vector1, Vector2Int _Vector2)
		{
			return new Vector2Int(_Vector1.X - _Vector2.X, _Vector1.Y - _Vector2.Y);
		}

		public static Vector2Int operator -(Vector2Int _Vector)
		{
			return new Vector2Int(-_Vector.X, -_Vector.Y);
		}

		public static Vector2Int operator *(Vector2Int _Vector, int _Value)
		{
			return new Vector2Int
			(
				_Vector.X * _Value,
				_Vector.Y * _Value
			);
		}

	#endregion

}

}