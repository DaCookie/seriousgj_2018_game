﻿using UnityEngine;

namespace MuffinTools
{

///<summary>
/// 
///		This interface is used by Vector3Methods.FindBarycentre
///	methods. With it, these methods can use a "weight" variable
///	for a barycentre calculation.
/// 
///</summary>
public interface IBarycentreElement
{

	#region Methods
	#endregion

	
	#region Accessors

		Vector3 Position { get; }

		float BarycentreWeight { get; }

	#endregion

}

}