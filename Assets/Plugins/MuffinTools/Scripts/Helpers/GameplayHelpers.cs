﻿using UnityEngine;

namespace MuffinTools
{

///<summary>
/// 
///		
/// 
///</summary>
public static class GameplayHelpers
{

	#region Obstacle detection

		/* 3D Methods */

		/// <summary>
		/// Checks if there's something in the given direction.
		/// </summary>
		public static bool IsSomethingInDirection
		(
			Vector3 _From,
			Vector3 _Direction,
			float _DetectionRange = Mathf.Infinity,
			int _LayerMask = Physics.DefaultRaycastLayers
		)
		{
			Ray ray = new Ray(_From, _Direction);
			return (Physics.Raycast(ray, _DetectionRange, _LayerMask));
		}

		/// <summary>
		/// Checks if there's something in the forward direction of the given Transform.
		/// </summary>
		public static bool IsSomethingForward
		(
			Transform _Transform,
			float _DetectionRange = Mathf.Infinity,
			int _LayerMask = Physics.DefaultRaycastLayers
		)
		{
			return IsSomethingInDirection(_Transform.position, _Transform.forward, _DetectionRange, _LayerMask);
		}

		/// <summary>
		/// Checks if there's something in the given direction, using a Sphere Cast.
		/// </summary>
		public static bool IsSomethingInDirectionSphere
		(
			Vector3 _From,
			Vector3 _Direction,
			float _SphereRadius = 0.5f,
			float _DetectionRange = Mathf.Infinity,
			int _LayerMask = Physics.DefaultRaycastLayers
		)
		{
			Ray ray = new Ray(_From, _Direction);
			return (Physics.SphereCast(ray, _SphereRadius, _DetectionRange, _LayerMask));
		}

		/// <summary>
		/// Checks if there's something in the forward direction of the given Transform, using a Sphere Cast.
		/// </summary>
		public static bool IsSomethingForwardSphere
		(
			Transform _Transform,
			float _SphereRadius = 0.5f,
			float _DetectionRange = Mathf.Infinity,
			int _LayerMask = Physics.DefaultRaycastLayers
		)
		{
			return IsSomethingInDirectionSphere(_Transform.position, _Transform.forward, _SphereRadius, _DetectionRange, _LayerMask);
		}

		/// <summary>
		/// Checks if theer's somethong below.
		/// </summary>
		public static bool IsOnFloor
		(
			Vector3 _From,
			float _DetectionRange,
			int _LayerMask = Physics.DefaultRaycastLayers
		)
		{
			return IsSomethingInDirection(_From, Vector3.down, _DetectionRange, _LayerMask);
		}

		/// <summary>
		/// Checks if there's something below, using a Sphere Cast.
		/// </summary>
		public static bool IsOnFloorSphere
		(
			Vector3 _From,
			float _SphereRadius,
			float _DetectionRange,
			int _LayerMask = Physics.DefaultRaycastLayers
		)
		{
			return IsSomethingInDirectionSphere(_From, Vector3.down, _SphereRadius, _DetectionRange, _LayerMask);
		}

		/* 2D Methods */

		/// <summary>
		/// Checks if there's something in the given direction.
		/// </summary>
		public static bool IsSomethingInDirection2D
		(
			Vector3 _From,
			Vector3 _Direction,
			float _DetectionRange = Mathf.Infinity,
			int _LayerMask = Physics.DefaultRaycastLayers
		)
		{
			return (Physics2D.Raycast(_From, _Direction, _DetectionRange, _LayerMask).collider != null);
		}

		/// <summary>
		/// Checks if there's something in the right direction of the given Transform.
		/// </summary>
		public static bool IsSomethingForward2D
		(
			Transform _Transform,
			float _DetectionRange = Mathf.Infinity,
			int _LayerMask = Physics.DefaultRaycastLayers
		)
		{
			return IsSomethingInDirection2D(_Transform.position, _Transform.right, _DetectionRange, _LayerMask);
		}

		/// <summary>
		/// Checks if there's something in the given direction, using a Circle Cast.
		/// </summary>
		public static bool IsSomethingInDirectionCircle2D
		(
			Vector3 _From,
			Vector3 _Direction,
			float _CircleRadius = 0.5f,
			float _DetectionRange = Mathf.Infinity,
			int _LayerMask = Physics.DefaultRaycastLayers
		)
		{
			return (Physics2D.CircleCast(_From, _CircleRadius, _Direction, _DetectionRange, _LayerMask).collider != null);
		}

		/// <summary>
		/// Checks if there's something in the right direction of the given Transform, using a Circle Cast.
		/// </summary>
		public static bool IsSomethingForwardCircle2D
		(
			Transform _Transform,
			float _CircleRadius = 0.5f,
			float _DetectionRange = Mathf.Infinity,
			int _LayerMask = Physics.DefaultRaycastLayers
		)
		{
			return IsSomethingInDirectionCircle2D(_Transform.position, _Transform.forward, _CircleRadius, _DetectionRange, _LayerMask);
		}

		/// <summary>
		/// Checks if theer's somethong below.
		/// </summary>
		public static bool IsOnFloor2D
		(
			Vector3 _From,
			float _DetectionRange,
			int _LayerMask = Physics.DefaultRaycastLayers
		)
		{
			return IsSomethingInDirection2D(_From, Vector3.down, _DetectionRange, _LayerMask);
		}

		/// <summary>
		/// Checks if there's something below, using a Sphere Cast.
		/// </summary>
		public static bool IsOnFloorCircle2D
		(
			Vector3 _From,
			float _SphereRadius,
			float _DetectionRange,
			int _LayerMask = Physics.DefaultRaycastLayers
		)
		{
			return IsSomethingInDirectionCircle2D(_From, Vector3.down, _SphereRadius, _DetectionRange, _LayerMask);
		}

	#endregion

}

}