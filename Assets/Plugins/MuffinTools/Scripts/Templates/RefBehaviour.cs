﻿using UnityEngine;

namespace MuffinTools
{

///<summary>
/// 
///		This class represents an object that has a reference to another component.
///		
///		You must override OnAwakeMessage() instead of Awake().
/// 
///</summary>

public abstract class RefBehaviour<T> : MonoBehaviour
	where T : Component
{

	#region Attributes

		// References

		private T m_Ref = null;

	#endregion

	
	#region Engine Methods

		private void Awake()
		{
			m_Ref = GetComponent<T>();
			OnAwakeMessage();
		}

	#endregion

	
	#region Protected Methods

		protected virtual void OnAwakeMessage()
		{

		}

	#endregion

	
	#region Accessors

		public T Ref
		{
			get { return m_Ref; }
		}

	#endregion

}

}